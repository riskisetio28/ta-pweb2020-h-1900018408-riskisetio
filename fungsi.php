<?php
$arrNilai=array("Rizki"=>80, "Riski"=>95, "Risky"=>75, "Rizky"=>85);
echo "<b>Array sebelum diurutkan </b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan sort ()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan ksort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>
<!-- PERTEMUAN 12 -->