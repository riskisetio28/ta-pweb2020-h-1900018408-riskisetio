<?php
$arrNilai = array("Rizki"=>80, "Riski"=>95, "Risky"=>75, "Rizky"=>85);
echo "Menampilkan Isi Array Asosiatif dengan Foreach : <br>";
foreach($arrNilai as $nama=>$nilai){
	echo "Nilai $nama = $nilai <br>";
}
reset($arrNilai);
echo "<br> Menampilkan Isi Array Asosiatif dengan While dan List : <br>";
while(list($nama,$nilai)=each($arrNilai)){
	echo "Nilai $nama=$nilai<br>";
}
?>
<!-- PERTEMUAN 12 -->